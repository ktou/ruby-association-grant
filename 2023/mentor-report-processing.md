# 2023年度Rubyアソシエーション開発助成 メンター報告書

* プロジェクト：CRuby 用 Processing Gem の、本家 Processing との互換性向上に向けた取り組み
* 応募者：tokujiros
* メンター：須藤功平

## プロジェクト概要

[応募者の最終報告書](https://gist.github.com/xord/c464669d41f37f4858a193c658e9a705#21-%E6%A6%82%E8%A6%81)より引用する。

> クリエイティブコーディングの世界では [Processing](https://processing.org/) や [p5.js](https://p5js.org/) のようなフレームワークが広く利用されています。これらを利用することで、フレームワーク利用者はグラフィックスと視覚的な表現を駆使したアートやインタラクティブなアプリケーションを簡単に作成することができます。
>
> 本プロジェクト開始前の時点で、その Processing/p5.js の API と互換性のある CRuby 向けの Processing Gem をゼロから開発しており、Processing の主要な機能の約7割から8割は実装済みでした。
>
> 本プロジェクトではその残りの未実装となっている機能を実装することで、Ruby プログラマーが、広く知られた Processing API を使用して手軽にグラフィックスプログラミングを行えるよう支援するものです。

## 計画の達成状況

本プロジェクトは、ほぼ当初の計画通りの成果を達成した。計画した成果は次のとおりである。

1. Processing Gemの本家Processingとの互換性向上
2. Rubyによるグラフィックスプログラミングを広める活動

### 1. Processing Gemの本家Processingとの互換性向上

1.の詳細は[応募者の最終報告書](https://gist.github.com/xord/c464669d41f37f4858a193c658e9a705#3-%E9%96%8B%E7%99%BA%E8%A8%88%E7%94%BB)にある。難易度・優先度に応じて5つのグループに分けてある。

実際の1.の成果の詳細は[応募者の最終報告書](https://gist.github.com/xord/c464669d41f37f4858a193c658e9a705#4-%E9%96%8B%E7%99%BA%E3%81%AE%E6%88%90%E6%9E%9C)にある。この中で言及されている通り、以下の当初計画していた機能は実装しなかった。「`Shape#stroke`の頂点色指定」の実装難易度に関する理由は妥当性の判断が難しいが、他の理由は妥当なものである。

* `Shape#stroke`の頂点色指定：内部構造上実装難易度が高かったため
* `preload`：JavaScript版Processingには必要だがRuby版であるProcessing gemには不要なため
* `Shape#modelX`：描画性能が落ちる可能性が示唆されたため
* `Shape#modelY`：描画性能が落ちる可能性が示唆されたため
* `Shape#modelZ`：描画性能が落ちる可能性が示唆されたため
* `Shape#screenX`：描画性能が落ちる可能性が示唆されたため
* `Shape#screenY`：描画性能が落ちる可能性が示唆されたため
* `Shape#screenZ`：描画性能が落ちる可能性が示唆されたため
* `deviceOrientation`：本家Processingには存在しないため
* `accelerationX`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `accelerationY`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `accelerationZ`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `pAccelerationX`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `pAccelerationY`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `pAccelerationZ`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `rotationX`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `rotationY`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `rotationZ`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `pRotationX`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `pRotationY`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `pRotationZ`：本家Processingには存在せず、JavaScript版ProcessingではMac/iPhone上では機能しないため
* `turnAxis`：本家Processingには存在しないため
* `deviceTurned`：本家Processingには存在しないため
* `deviceMoved`：本家Processingには存在しないため
* `deviceShaked`：本家Processingには存在しないため

当初の計画は機能の実装にフォーカスしていたが、本プロジェクトではJavaScript版Processingを利用した単体テストの整備も進め、リグレッションを抑えて機能の開発を進められる環境も整備した。

### 2. Rubyによるグラフィックスプログラミングを広める活動

2.のために[Ruby Advent Calendar 2023](https://qiita.com/advent-calendar/2023/ruby)の記事の1つとして本プロジェクトを紹介する以下の記事を執筆した。

[Processing ベースの2Dゲームエンジン for CRuby の紹介](https://zenn.dev/tokujiros/articles/7f0b44a6b7e2a6)

また、本プロジェクトの成果を利用したiOSアプリ[RubySketch](https://apps.apple.com/jp/app/rubysketch/id1491477639)をバージョンアップし、より多くの人が本プロジェクトの成果を使ってRubyによるグラフィックスプログラミングを楽しめる環境を整備した。

## メンターとして果たした役割

本プロジェクトのメンターとして次の役割を果たした。

* 計画作成の支援
* メンターの既存の知見を活かした口頭での質疑応答
* 中間報告書のレビュー
* 最終報告書のレビュー
* 本プロジェクトの成果を今後どのように活かせるかの議論およびそれを推進するための関係者の紹介

## 今後の期待

クリエイティブコーディングはRubyがまだ広く使われていない領域であるので、本プロジェクトの成果で新しくRubyユーザーが増えるあるいはRubyでクリエイティブコーディングを楽しむ人が増えることを期待したい。

当初の計画通り、本プロジェクトにより本家Processing・JavaScript版Processingとの互換性が向上した。3D機能など高度な機能は未実装だが本家Processing・JavaScript版Processingでできる多くのクリエイティブコーディングをProcessing gemで実現できるようになったはずである。これにより新しいRubyユーザーおよびRubyでクリエイティブコーディングを楽しむ人が増えることを期待したい。

なお、Processing gem同様にRubyでProcessingのAPIを使ってクリエイティブコーディングできる[p5.rb](https://p5rb.ongaeshi.me/)が存在する。Processing gemは描画処理から独自に実装しているが、p5.rbはruby.wasmとJavaScript版Processingを使った実装となっている。p5.rbはWebブラウザー上で実行することを想定していて、Processing gemはMac/iPhone/iPad上で実行することを想定している。ユーザー層が重複していないので、p5.rbで書いたコードをProcessing gemで簡単に動かせる、あるいはその逆など、p5.rb/Processing gemの相乗効果でよりRubyユーザーおよびRubyでクリエイティブコーディングを楽しむ人が増えることを期待したい。

最終報告書ではWindows対応も言及されている（現在はMac/iOS上でしか動かない）が、それもRubyユーザーおよびRubyでクリエイティブコーディングを楽しむ人が増えることに寄与するだろう。

## まとめ

本プロジェクトでは、RubyでクリエイティブコーディングをするためのライブラリーProcessing gemの機能拡張をした。本プロジェクト開始前の時点で本家Processing・JavaScript版Processingの多くの機能は使える状態だったが、本プロジェクトで通常のユースケースで必要になる機能をほぼすべてカバーできる状態まで改良した。

同様の機能を提供するp5.rbはWebブラウザー上でRubyを使ってクリエイティブコーディングできる。一方、本プロジェクトのProcessing gemはMac/iPhone/iPad上でRubyを使ってクリエイティブコーディングできる。本プロジェクトにより、さらに多くの環境でRubyを使ってクリエイティブコーディングできるようになった。
